import {
  Race,
  RacingTrack,
  UserVehicle,
  UserLapTime,
  UserRacer,
  User,
  Vehicle,
} from '../models'
import { Sequelize } from 'sequelize-typescript'
import * as _ from 'lodash'
import * as schedule from 'node-schedule'
import { promisify, delay } from 'bluebird'
import { UserRacerService } from '../services/UserRacerService'

export default class RaceController {
  private race: Race

  constructor() {}

  async initRace(
    vehicles: UserVehicle[],
    league: number,
  ): Promise<RaceController> {
    const laps = _.random(1, 5)
    const race = await Race.create({
      laps,
    }).then(race => {
      return race
    })

    const userRacers = vehicles.map(v => {
      return { userId: v.ownerId, raceId: race.id, vehicleId: v.id }
    })

    await UserRacer.scope('full').bulkCreate(userRacers)

    await UserRacerService.getRacersByRace(race).then(racers => {
      racers.map(async racer => {
        const user = racer.user
        const profile = user.profile
        profile.races++
        user.status = 'RACE'
        await user.save()
        await profile.save()
      })
    })
    console.log(`Race ${race.id} was created`)
    this.race = race
    return this
  }

  private async decreaseFuel(racer: UserRacer): Promise<User> {
    const consumption = racer.vehicle.vehicle.fuelConsumption * 10
    console.log(
      `User ${racer.userId} fuel decreased by ${consumption}, vehicle ${
        racer.vehicle.vehicle.name
      }`,
    )
    racer.user.leftFuel -= consumption
    return await racer.user.save()
  }

  private async updateExp(racer: UserRacer): Promise<void> {
    const exp = 5 + _.random(-1, 1) * 1
    await this.addExp(racer.user, exp)
  }

  private async addExp(user: User, exp: number): Promise<void> {
    user.experience += exp
    console.log(`Added ${exp} to user ${user.id}`)
    // TODO add leveling up
    await user.save()
  }

  private async updateLap(lap: number): Promise<void> {
    await UserRacerService.getRacersByRace(this.race).then(async racers => {
      const lapsTime = await racers.map(async racer => {
        const lapSpeed = await this.getSpeed(racer.vehicle.vehicle, racer).then(
          speed => speed,
        )
        const lapTime = await this.getLapTime(lapSpeed).then(time => time)
        racer.mainTime += lapTime
        const user = await this.decreaseFuel(racer)
        await this.updateExp(racer)
        console.log(
          `User ${
            user.id
          } updated lap time to ${lapTime} on lap ${lap} with speed ${lapSpeed}`,
        )
        await racer.save()
        return { racerId: racer.id, lap: lap, time: lapTime, speed: lapSpeed }
      })

      Promise.all(lapsTime).then(async times => {
        await UserLapTime.bulkCreate(times)
      })
    })
  }

  // TODO laps intervals and wait before race
  async startRace(): Promise<void> {
    await delay(5000)
    console.log(`Race ${this.race.id} was started`)
    const laps = _.range(0, this.race.laps).map(async lap => {
      return new Promise(resolve => {
        setTimeout(async () => {
          await this.updateLap(lap)
          resolve()
        }, lap * 5000)
      })
    })
    Promise.all(laps).then(async () => {
      await this.endRace()
      console.log(`Race ${this.race.id} has ended`)
    })
  }

  private async endRace(): Promise<void> {
    await UserRacerService.getRacersByRace(this.race).then(racers => {
      const MONEY = [1000, 500, 250]
      const MEDAL = ['gold', 'silver', 'bronze']
      const EXP_FOR_PLACE = [50, 30, 20]
      racers.slice(0, 3).map(async (racer, index) => {
        const moneyForRace = MONEY[index] * 1
        racer.user.money += moneyForRace
        racer.user.profile[MEDAL[index]] += 1
        await racer.user.profile.save()
        await this.addExp(racer.user, EXP_FOR_PLACE[index])
      })

      racers.map(async racer => {
        const user = racer.user
        const profile = user.profile
        user.status = 'REST'
        user.isParticipating = false
        await user.save()
        await profile.save()
      })
    })
  }

  private async getSpeed(vehicle: Vehicle, racer: UserRacer) {
    const level = racer.user.level

    const randomDiff = _.random(-0.125 + level * 0.003, 0.125)
    const randomDiffCoeff = 1 + randomDiff

    const accSpeed = (vehicle.acceleration / 10 + 1) * 100
    const maxSpeed = (vehicle.maxSpeed / 10 + 1) * 100

    const trackSpeed = this.race.racingTrack.speedIndex
    const accSpeedCoeff = Math.round((10 - trackSpeed) / 10)
    const maxSpeedCoeff = Math.round(1 - accSpeedCoeff)

    const sp = accSpeed + accSpeedCoeff + maxSpeed * maxSpeedCoeff
    let speed = sp * randomDiffCoeff
    speed += speed * (trackSpeed * 0.02)
    speed = Math.round(speed)
    return speed
  }

  private async getLapTime(speed) {
    const trackLength = this.race.racingTrack.length
    const lapTime = Math.round(trackLength / speed)
    return lapTime
  }
}
