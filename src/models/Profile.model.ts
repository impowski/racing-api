import {
  Table,
  Scopes,
  Model,
  PrimaryKey,
  Column,
  Unique,
  Default,
  DataType,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript'

import User from './User.model'

@Table({
  timestamps: true,
})
@Scopes({
  withOwner: {
    include: [() => User]
  }
})
export default class Profile extends Model<Profile> {
  @PrimaryKey
  @Unique
  @Default(DataType.UUIDV4)
  @Column(DataType.UUID)
  id: string

  @Default(0)
  @Column
  races: number

  @Default(0)
  @Column
  gold: number

  @Default(0)
  @Column
  silver: number

  @Default(0)
  @Column
  bronze: number

  @ForeignKey(() => User)
  @Column(DataType.UUID)
  ownerId: string

  @BelongsTo(() => User, 'ownerId')
  owner?: User
}
