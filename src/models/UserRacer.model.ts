import {
  Table,
  Model,
  PrimaryKey,
  Column,
  Unique,
  Default,
  DataType,
  HasOne,
  AfterCreate,
  AfterBulkCreate,
  ForeignKey,
  Scopes,
  HasMany,
  BelongsTo,
} from 'sequelize-typescript'

import User from './User.model'
import Race from './Race.model'
import UserVehicle from './UserVehicle.model'
import * as _ from 'lodash'
import Vehicle from './Vehicle.model';
import Profile from './Profile.model';
import UserLapTime from './UserLapTime.model';

@Table({
  timestamps: true,
})
@Scopes({
  full: {
    include: [{
      model: () => UserVehicle,
      include: [{
        model: () => Vehicle
      }]
    }, {
      model: () => User,
      include: [{
        model: () => Profile
      }]
    }],
  }
})
export default class UserRacer extends Model<UserRacer> {
  @PrimaryKey
  @Unique
  @Default(DataType.UUIDV4)
  @Column(DataType.UUID)
  id: string

  @Default(_.random(0, 5))
  @Column
  mainTime: number

  @ForeignKey(() => User)
  @Column(DataType.UUID)
  userId: string

  @BelongsTo(() => User, 'userId')
  user: User

  @ForeignKey(() => UserVehicle)
  @Column(DataType.UUID)
  vehicleId: string

  @BelongsTo(() => UserVehicle, 'vehicleId')
  vehicle: UserVehicle

  @ForeignKey(() => Race)
  @Column(DataType.UUID)
  raceId: string

  @BelongsTo(() => Race, 'raceId')
  race: Race

  @HasMany(() => UserLapTime)
  lapsTime: UserLapTime[]
}
