import {
  Table,
  Model,
  PrimaryKey,
  Unique,
  Column,
  Default,
  DataType,
  BelongsTo,
  HasOne,
  HasMany,
  Scopes,
  DefaultScope,
} from 'sequelize-typescript'

import Race from './Race.model'

@Table({
  timestamps: true,
})
@DefaultScope({
  attributes: [
    'id',
    'name',
    'country',
    'turns',
    'length',
    'straights',
    'speedIndex',
  ],
})
@Scopes({
  withRaces: {
    include: [() => Race],
  },
})
export default class RacingTrack extends Model<RacingTrack> {
  @PrimaryKey
  @Unique
  @Default(DataType.UUIDV4)
  @Column(DataType.UUID)
  id: string

  @Default('Some Race')
  @Column
  name: string

  @Default('US')
  @Column
  country: string

  @Default('3350')
  @Column
  length: number

  @Default(5)
  @Column
  turns: number

  @Default(5)
  @Column
  straights: number

  @Default(5)
  @Column
  speedIndex: number

  @HasMany(() => Race)
  races: Race[]
}
