import {
  Table,
  Model,
  PrimaryKey,
  Column,
  Unique,
  Default,
  DataType,
  HasOne,
  AfterCreate,
  AfterBulkCreate,
  ForeignKey,
  Scopes,
  BelongsTo,
  HasMany,
  DefaultScope,
  BeforeCreate,
  Sequelize,
} from 'sequelize-typescript'

import RacingTrack from './RacingTrack.model'
import UserRacer from './UserRacer.model';

@Table({
  timestamps: true,
})
@Scopes({
  full: {
    include: [() => RacingTrack, () => UserRacer]
  },
  withRacingTrack: {
    include: [() => RacingTrack],
  },
  withRacers: {
    include: [() => UserRacer]
  }
})
export default class UserLapTime extends Model<UserLapTime> {
  @PrimaryKey
  @Unique
  @Default(DataType.UUIDV4)
  @Column(DataType.UUID)
  id: string

  @Column
  lap: number

  @Column
  time: number

  @Column
  speed: number

  @ForeignKey(() => UserRacer)
  @Column(DataType.UUID)
  racerId: string

  @BelongsTo(() => UserRacer, 'racerId')
  racer: UserRacer
}
