import {
  Table,
  Model,
  PrimaryKey,
  Column,
  Unique,
  Default,
  DataType,
  HasOne,
  AfterCreate,
  AfterBulkCreate,
  ForeignKey,
  Scopes,
  BelongsTo,
  HasMany,
  DefaultScope,
  BeforeCreate,
  Sequelize,
} from 'sequelize-typescript'

import RacingTrack from './RacingTrack.model'
import UserRacer from './UserRacer.model';

@Table({
  timestamps: true,
})
@Scopes({
  full: {
    include: [() => RacingTrack, () => UserRacer]
  },
  withRacingTrack: {
    include: [() => RacingTrack],
  },
  withRacers: {
    include: [() => UserRacer]
  }
})
export default class Race extends Model<Race> {
  @PrimaryKey
  @Unique
  @Default(DataType.UUIDV4)
  @Column(DataType.UUID)
  id: string

  @Column
  laps: number

  @ForeignKey(() => RacingTrack)
  @Column(DataType.UUID)
  racingTrackId: string

  @BelongsTo(() => RacingTrack, 'racingTrackId')
  racingTrack: RacingTrack

  @HasMany(() => UserRacer)
  racers: UserRacer[]

  @BeforeCreate
  static async getRandomRacingTrack(race: Race) {
    const racingTrack = await RacingTrack.findOne({
      order: Sequelize.fn('random')
    }).then(racingTrack => racingTrack)

    race.racingTrack = racingTrack
    race.racingTrackId = racingTrack.id
  }
}
