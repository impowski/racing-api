import {
  Table,
  Model,
  PrimaryKey,
  Column,
  Unique,
  Default,
  DataType,
  HasOne,
  AfterCreate,
  AfterBulkCreate,
  ForeignKey,
  Scopes,
  HasMany,
} from 'sequelize-typescript'
import Profile from './Profile.model'
import UserVehicle from './UserVehicle.model'
import UserRacer from './UserRacer.model'
import Vehicle from './Vehicle.model'

@Table({
  timestamps: true,
})
@Scopes({
  full: {
    include: [
      () => Profile,
      () => UserRacer,
      {
        model: () => UserVehicle,
        include: [() => Vehicle],
      },
    ],
  },
  withProfile: {
    include: [() => Profile],
  },
  withRacers: {
    include: [() => UserRacer],
  },
  withVehicles: {
    include: [() => UserVehicle],
  },
  lang: {
    attributes: ['id', 'lang'],
  },
})
export default class User extends Model<User> {
  @PrimaryKey
  @Unique
  @Default(DataType.UUIDV4)
  @Column(DataType.UUID)
  id: string

  @Unique
  @Column
  username: string

  @Default(0)
  @Column
  money: number

  @Default(1000)
  @Column
  fuel: number

  @Default(1000)
  @Column
  leftFuel: number

  @Default(1)
  @Column
  level: number

  @Default(0)
  @Column
  experience: number

  @Default(false)
  @Column
  isBanned: boolean

  @Default(false)
  @Column
  isDeleted: boolean

  @Default('ru')
  @Column(DataType.STRING(3))
  lang: string

  @Default('REST')
  @Column(DataType.ENUM('REST', 'RACE'))
  status: 'REST'|'RACE'

  @Default(false)
  @Column(DataType.BOOLEAN)
  isParticipating: boolean

  @HasOne(() => Profile)
  profile: Profile

  @HasMany(() => UserVehicle)
  vehicles?: UserVehicle[]

  @HasMany(() => UserRacer)
  racers: UserRacer[]

  @AfterCreate
  static async createProfile(user: User) {
    await Profile.create({
      owner: user,
    })
  }

  @AfterBulkCreate
  static async bulkCreateProfiles(users: User[]) {
    await Profile.bulkCreate(
      users.map(u => {
        return { ownerId: u.id }
      }),
    )
  }
}
