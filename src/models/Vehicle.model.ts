import {
  Table,
  Model,
  PrimaryKey,
  Column,
  Unique,
  Default,
  DataType,
  HasOne,
  AfterCreate,
  AfterBulkCreate,
  ForeignKey,
  Scopes,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript'

import UserVehicle from './UserVehicle.model'

@Table({
  timestamps: true,
})
export default class Vehicle extends Model<Vehicle> {
  @PrimaryKey
  @Unique
  @Default(DataType.UUIDV4)
  @Column(DataType.UUID)
  id: string

  @Default('Some Car')
  @Column
  name: string

  @Default(1)
  @Column
  maxSpeed: number

  @Default(1)
  @Column
  acceleration: number

  @Default(1)
  @Column
  fuelConsumption: number

  @HasMany(() => UserVehicle)
  owners: UserVehicle[]
}
