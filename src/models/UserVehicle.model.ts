import {
  Table,
  Model,
  PrimaryKey,
  Column,
  Unique,
  Default,
  DataType,
  HasOne,
  AfterCreate,
  AfterBulkCreate,
  ForeignKey,
  Scopes,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript'

import User from './User.model'
import UserRacer from './UserRacer.model'
import Vehicle from './Vehicle.model'

@Table({
  timestamps: true,
})
@Scopes({
  withVehicle: {
    include: [() => Vehicle]
  },
  withOwner: {
    include: [() => User]
  },
  withParticipatingOwner: {
    include: [{
      model: () => User,
      where: {
        status: 'REST',
        isParticipating: true
      }
    }],
  }
})
export default class UserVehicle extends Model<UserVehicle> {
  @PrimaryKey
  @Default(DataType.UUIDV4)
  @Unique
  @Column(DataType.UUID)
  id: string

  @ForeignKey(() => User)
  @Column(DataType.UUID)
  ownerId: string

  @BelongsTo(() => User, 'ownerId')
  owner: User

  @HasMany(() => UserRacer)
  racers: UserRacer[]

  @ForeignKey(() => Vehicle)
  @Column(DataType.UUID)
  vehicleId: string

  @BelongsTo(() => Vehicle, 'vehicleId')
  vehicle: Vehicle

  @Default(false)
  @Column
  isSelected: boolean

  @Default(1)
  @Column
  level: number
}
