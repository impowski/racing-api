import { user } from './user'
import { race } from './race'

export const Query = {
  ...user,
  ...race,
}

export default Query
