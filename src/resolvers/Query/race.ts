// import gql from 'graphql-tag'

export const race = {
  async getRaceInfo(_, { raceId }) {
    // return await ctx.db.query.userRacers({
    //   where: {
    //     race: {
    //       id: raceId
    //     }
    //   },
    //   orderBy: 'mainTime_ASC'
    // }, info)
  },
  async getRaceQueue(_, args, { models }) {
    const inQueue = await models.UserVehicle.count({
      where: {
        isSelected: true,
      },
      include: [
        {
          model: models.User,
          where: {
            status: models.Status.REST,
            isParticipating: true
          },
        },
      ],
    })

    const inRaces = await models.User.count({
      where: {
        status: models.Status.RACE
      }
    })

    return { inQueue, inRaces }
  },
}
