// import { Context } from '../../utils'
// import gql from 'graphql-tag'
export const user = {
  async users (parent, args, { models }) {
    const users = await models.User.scope('full').findAndCountAll({
      where: {
        isDeleted: false
      },
    })
    return { count: users.count, users: users.rows }
  },
  async getUserProfile (parent, { id }, { models }) {
    const profile = await models.Profile.scope('withOwner').findOne({
      where: {
        ownerId: id
      }
    })
    return profile
  },
  async getUserVehicles (parent, { id }, { models }) {
    const vehicles = await models.UserVehicle.findAll({
      where: {
        ownerId: id
      }
    })
    
    return vehicles
  }
}