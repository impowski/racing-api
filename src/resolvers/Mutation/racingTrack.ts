export const racingTrack = {
  async createNewRacingTrack(parent, args, { models }) {
    return await models.RacingTrack.create()
      .then(() => {
        return { success: true }
      }).catch(error => {
        console.log(error.original.detail)
        return { success: false }
      })
  }
}