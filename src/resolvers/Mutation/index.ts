import { user } from './user'
import { race } from './race'
import { vehicle } from './vehicle'
import { racingTrack } from './racingTrack'

export const Mutation = {
  ...user,
  ...race,
  ...vehicle,
  ...racingTrack
}

export default Mutation