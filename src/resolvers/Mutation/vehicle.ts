import { Sequelize } from 'sequelize-typescript'

export const vehicle = {
  async buyVehicle(parent, { id, car }, { models }) {},
  async selectVehicle(parent, { id, ownerId }, { models }) {
    const vehicle = await models.UserVehicle.findOne({
      where: {
        id,
        ownerId
      }
    })

    if (!vehicle) {
      console.error('Tried to select non-existing vehicle')
      return { success: false }
    }

    if (!vehicle.isSelected) {
      await models.UserVehicle.update(
        {
          isSelected: false,
        },
        {
          where: {
            isSelected: true,
            ownerId,
          },
        },
      ).then(() => console.log(`Deselected all selected vehicles of user ${ownerId}`))

      return await models.UserVehicle.update(
        {
          isSelected: true,
        },
        {
          where: {
            id,
            ownerId,
          },
        },
      )
        .then(() => {
          console.log(`Selected vehicle ${id}`)
          return { success: true }
        })
        .catch(() => {
          return { success: false }
        })
    }
    return { success: true }
  },
}
