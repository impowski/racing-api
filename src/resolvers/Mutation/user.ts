import * as _ from 'lodash'
import { Sequelize } from 'sequelize-typescript'

export const user = {
  // TODO create additional relations
  async newUser(parent, { username }, { models }) {
    await models.User.findOrCreate({
      where: {
        username,
      },
      defaults: {
        isDeleted: false,
      },
    }).spread((user, created) => {
      return user.get()
    })

    return { success: true }
  },
  async changeNickname(parent, { id, username }, { models }) {
    return await models.User.update(
      {
        username,
      },
      {
        where: {
          id,
        },
      },
    )
      .then(() => {
        console.log(`User ${id} succesfully changed nickname to ${username}`)
        return { success: true }
      })
      .catch(error => {
        console.log(error.original.detail)
        return { success: false }
      })
  },
  async changeLanguage(parent, { id, lang }, { models }) {
    return await models.User.update(
      {
        lang,
      },
      {
        where: {
          id,
        },
      },
    )
      .then(() => {
        console.log(`User ${id} changed language to ${lang}`)
        return { success: true }
      })
      .catch(error => {
        console.log(error.original.detail)
        return { success: false }
      })
  },
  async changeStatus(parent, { id, status }, { models }) {
    return await models.User.update(
      {
        status,
      },
      {
        where: {
          id,
        },
      },
    )
      .then(() => {
        console.log(`User ${id} changed status to ${status}`)
        return { success: true }
      })
      .catch(error => {
        console.log(error.original.detail)
        return { success: false }
      })
  },
  async joinRace(_, { id }, { models }) {
    const vehicle = await models.UserVehicle.scope('withVehicle').find({
      where: {
        isSelected: true
      },
      include: [{
        model: models.User,
        where: {
          id
        }
      }]
    })

    if (!vehicle) {
      console.log(JSON.stringify(vehicle))
      console.log(`User ${id} tried to join race without a vehicle`)
      return { success: false }
    }

    return { success: true }
  }
}
