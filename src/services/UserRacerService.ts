import { UserRacer, Race } from '../models'
import { Sequelize } from 'sequelize-typescript';

export class UserRacerService {
  constructor() {}

  static async getRacersByRace(race: Race): Promise<UserRacer[]> {
    return await UserRacer.scope('full').findAll({
      where: {
        raceId: race.id
      },
      order: [['mainTime', 'ASC']]
    })
  }

  static async changeRacersStatusByRace(race: Race): Promise<[number, UserRacer[]]> {
    return await UserRacer.update({
      user: {
        status: Sequelize.literal(`status`).val === 'REST' ? 'RACE' : 'REST',
        profile: {
          races: Sequelize.literal(`status`).val === 'REST' ? Sequelize.literal(`races + 1`) : Sequelize.literal(`races`)
        }
      }
    }, {
      where: {
        raceId: race.id
      }
    })
  }
}