import { GraphQLServer } from 'graphql-yoga'
import { resolvers } from './resolvers'
import { Sequelize } from 'sequelize-typescript'
import * as _ from 'lodash'
import * as morgan from 'morgan'
import * as schedule from 'node-schedule'

import * as models from './models'
import RaceController from './controllers/RaceController'

const db = new Sequelize({
  logging: true,
  benchmark: true,
  name: 'racing',
  dialect: 'postgres',
  host: 'localhost',
  username: 'postgres',
  password: '',
  modelPaths: [__dirname + '/models/*.model.ts'],
})

const racingTrack = db
  .sync({ force: true })
  .then(() => {
    const users = _.range(10).map(num => {
      return { username: `testname${num}`, isParticipating: true }
    })
    return Promise.all([models.User.bulkCreate(users), models.Vehicle.create()])
  })
  .then(([users, vehicle]) => {
    const vehicles = users.map(u => {
      models.UserVehicle.create({
        ownerId: u.id,
        vehicleId: vehicle.id,
        isSelected: false
      })
      return { ownerId: u.id, vehicleId: vehicle.id, isSelected: true }
    })

    Promise.all([
      models.RacingTrack.create(),
      models.UserVehicle.bulkCreate(vehicles)
    ])
  })
  .then(() => {
    const j = schedule
    .scheduleJob('*/15 * * * * *', () => {
      console.log('Looking for ready racers...')
      models.UserVehicle.findAndCountAll({
        where: {
          isSelected: true,
        },
        limit: 15,
        include: [
          {
            model: models.User,
            where: {
              status: 'REST',
              isParticipating: true,
            },
          },
        ],
      }).then(usersVehicles => {
        if (usersVehicles.count >= 5) {
          const raceController = new RaceController()
          raceController.initRace(usersVehicles.rows, 2).then(rc => rc.startRace())
        }
      })
    })
    .schedule()
  })

// Add database to context

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: req => ({ ...req, db, models }),
})

server.express.use(morgan('combined'))

server.start(({ port }) =>
  console.log(`Server is running on http://localhost:${port}`),
)
